const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config()

const app = express()
const PORT = 3000

//Middleware
app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})
// DB connection notif
    // user connection property of mongoose
const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))

// SCHEMA
const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: [true, `Username is required`]
        },
        password: {
            type: String,
            required: [true, `Password is required`]
        }
    }
)
// MODEL
const User = mongoose.model(`User`, userSchema)

app.post('/signup', (req,res) => {
    const result = User.findOne({username: req.body.username,password: req.body.password}).then((result,err) => {
        console.log(result)

        if(result != null && result.username == req.body.username){
            return res.send(`Duplicate Task Found`)
        }
        else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save().then((savedUser,err) => {
                console.log(result)
                if(savedUser){
                    return res.status(200).send(`New User registered`)
                }
                else {
                    return res.status(500).send(err)
                }
            })
        }

    })
})

app.listen(PORT, ()=> console.log(`Connected to ${PORT}`))